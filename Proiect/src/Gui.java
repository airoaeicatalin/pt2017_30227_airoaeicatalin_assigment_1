import javafx.scene.control.*;


import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class Gui extends JFrame {

    private JTextField pas1, pas11;
    private JTextField pas2, pas22;
    private JTextField answer,answer1,answer2,answer3,answer4;
    private JTextField answerText,answerText1,answerText2,answerText3,answerText4;
    private JButton enterPas1, enterPas11;
    private JButton enterPas2, enterPas22;
    private JButton add, sub, multiply, div, integrate;
    private JButton derivata1, derivata2;
    private JPanel contentPanel;

    public Gui()
    {
        super("Calculator Polinoame");
        
        //_______________________________________PAS 1
        pas11 = new JTextField(null, 20);
        pas11.setBounds(10,20,300,20);
        pas11.setText("Introduceti numarul de termeni pentru primul polinom:");
        pas11.setEditable(false);
        pas1 = new JTextField(null, 20);
        pas1.setBounds(345,20,20,20);
        enterPas1 = new JButton("Enter");
        enterPas1.setBounds(400,20,80,20);
        enterPas11 = new JButton("Enter");
        enterPas11.setBounds(400, 80, 80, 20);
        //________________________________________PAS 2
        pas22 = new JTextField(null, 20);
        pas22.setBounds(10,140,300,20);
        pas22.setText("Introduceti numarul de termeni pentru al-2lea polinom:");
        pas22.setEditable(false);
        pas2 = new JTextField(null, 20);
        pas2.setBounds(345,140,20,20);
        enterPas2 = new JButton("Enter");
        enterPas2.setBounds(400,140,80,20);
        enterPas22 = new JButton("Enter");
        enterPas22.setBounds(400, 200, 80, 20);
        //_____________________________________________

        answer = new JTextField(null, 20);
        answer.setBounds(150, 300, 300, 20);
        answer.setEditable(false);

        answerText = new JTextField(null, 20);
        answerText.setBounds(10, 300, 100, 20);
        answerText.setText("Rezultat(+,-,/,*)");
        answerText.setEditable(false);

        answer1 = new JTextField(null, 20);
        answer1.setBounds(150, 330, 300, 20);
        answer1.setEditable(false);

        answerText1 = new JTextField(null, 20);
        answerText1.setBounds(10, 330, 100, 20);
        answerText1.setText("p1'(x) sau p1''(x)");
        answerText1.setEditable(false);

        answer2 = new JTextField(null, 20);
        answer2.setBounds(150, 360, 300, 20);
        answer2.setEditable(false);

        answerText2 = new JTextField(null, 20);
        answerText2.setBounds(10, 360, 100, 20);
        answerText2.setText("p2'(x) sau p2''(x)");
        answerText2.setEditable(false);
        
        answer3 = new JTextField(null, 20);
        answer3.setBounds(150, 390, 300, 20);
        answer3.setEditable(false);

        answerText3 = new JTextField(null, 20);
        answerText3.setBounds(10, 390, 100, 20);
        answerText3.setText("Integrate (p1(x))");
        answerText3.setEditable(false);
        
        answer4 = new JTextField(null, 20);
        answer4.setBounds(150, 420, 300, 20);
        answer4.setEditable(false);
        
        answerText4 = new JTextField(null, 20);
        answerText4.setBounds(10, 420, 100, 20);
        answerText4.setText("Integrate (p2(x))");
        answerText4.setEditable(false);



        add = new JButton("+");
        add.setBounds(20,570,110,50);
        sub = new JButton("-");
        sub.setBounds(170,570,110,50);
        div = new JButton("/");
        div.setBounds(320,570,110,50);
        multiply = new JButton("*");
        multiply.setBounds(470, 570, 110, 50);
        derivata1 = new JButton("p'(x)");
        derivata1.setBounds(20,490,110, 50);
        derivata2 = new JButton("p''(x)");
        derivata2.setBounds(470,490,110,50);
        integrate = new JButton("Integrate(p(x)");
        integrate.setBounds(230,490,130,50);
       

        Numbers n = new Numbers();

        add.addActionListener(n);
        sub.addActionListener(n);
        multiply.addActionListener(n);
        div.addActionListener(n);
        enterPas1.addActionListener(n);
        enterPas2.addActionListener(n);
        enterPas11.addActionListener(n);
        enterPas22.addActionListener(n);
        derivata1.addActionListener(n);
        derivata2.addActionListener(n);
        integrate.addActionListener(n);

        contentPanel = new JPanel();
        contentPanel.setBackground(Color.WHITE);
        contentPanel.setLayout(new FlowLayout());

    
        contentPanel.add(pas1, BorderLayout.SOUTH);
        contentPanel.add(pas11, BorderLayout.SOUTH);
        contentPanel.add(pas2, BorderLayout.SOUTH);
        contentPanel.add(pas22, BorderLayout.SOUTH);
        contentPanel.add(answer, BorderLayout.SOUTH);
        contentPanel.add(answer1,BorderLayout.SOUTH);
        contentPanel.add(answer2, BorderLayout.SOUTH);
        contentPanel.add(answer3, BorderLayout.SOUTH);
        contentPanel.add(answer4, BorderLayout.SOUTH);
        contentPanel.add(answerText,BorderLayout.SOUTH);
        contentPanel.add(answerText1,BorderLayout.SOUTH);
        contentPanel.add(answerText2,BorderLayout.SOUTH);
        contentPanel.add(answerText3,BorderLayout.SOUTH);
        contentPanel.add(answerText4,BorderLayout.SOUTH);
        contentPanel.add(enterPas1);
        contentPanel.add(enterPas11);
        contentPanel.add(enterPas2);
        contentPanel.add(enterPas22);
        contentPanel.add(add);
        contentPanel.add(sub);
        contentPanel.add(multiply);
        contentPanel.add(div);
        contentPanel.add(derivata1);
        contentPanel.add(derivata2);
        contentPanel.add(integrate);


        this.setContentPane(contentPanel);

    }

    private class Numbers implements ActionListener {
        JTextField[] pas1Coef = new JTextField[10];
        JTextField[] pas2Coef = new JTextField[10];
        JTextField plus;
        JTextField xPatrat;
        Polinom p1 = new Polinom(0,0);
        Polinom p2 = new Polinom(0,0);

        public void actionPerformed(ActionEvent event) {
            JButton src = (JButton) event.getSource();
            if (src.equals(enterPas1)) {
                int x = 20;
                int nrCoefPol1 = Integer.parseInt(pas1.getText());
                for (int i = 0; i < nrCoefPol1 * 2; i++) {
                    if (i % 2 == 0) {
                        xPatrat = new JTextField(null, 20);
                        xPatrat.setName("");
                        xPatrat.setText("x^");
                        xPatrat.setEditable(false);
                        xPatrat.setBounds(x + 20, 80, 20, 20);
                        contentPanel.add(xPatrat);
                    } else if (i % 2 != 0 && i < nrCoefPol1 * 2 - 1) {
                        plus = new JTextField(null, 20);
                        plus.setName("");
                        plus.setText("+");
                        plus.setEditable(false);
                        plus.setBounds(x + 20, 80, 20, 20);
                        contentPanel.add(plus);
                    }
                    if (i % 2 == 0) {
                        pas1Coef[i] = new JTextField(null, 20);
                        pas1Coef[i].setText(" ");
                        pas1Coef[i].setBounds(x, 80, 20, 20);
                        x = x + 40;
                        contentPanel.add(pas1Coef[i]);
                        pas1Coef[i].setText("");
                    } else {
                        pas1Coef[i] = new JTextField(null, 20);
                        pas1Coef[i].setText(" ");
                        pas1Coef[i].setBounds(x, 70, 15, 15);
                        x = x + 40;
                        contentPanel.add(pas1Coef[i]);
                        pas1Coef[i].setText("");
                    }
                }
            }


            if (src.equals(enterPas2)) {
                int y = 20;
                int nrCoefPol2 = Integer.parseInt(pas2.getText());
                for (int i = 0; i < nrCoefPol2 * 2; i++) {
                    if (i % 2 == 0) {
                        xPatrat = new JTextField(null, 20);
                        xPatrat.setName("");
                        xPatrat.setText("x^");
                        xPatrat.setEditable(false);
                        xPatrat.setBounds(y + 20, 200, 20, 20);
                        contentPanel.add(xPatrat);
                    } else if (i % 2 != 0 && i < nrCoefPol2 * 2 - 1) {
                        plus = new JTextField(null, 20);
                        plus.setName("");
                        plus.setText("+");
                        plus.setEditable(false);
                        plus.setBounds(y + 20, 200, 20, 20);
                        contentPanel.add(plus);
                    }
                    if (i % 2 == 0) {
                        pas2Coef[i] = new JTextField(null, 20);
                        pas2Coef[i].setText(" ");
                        pas2Coef[i].setBounds(y, 200, 20, 20);
                        y = y + 40;
                        contentPanel.add(pas2Coef[i]);
                        pas2Coef[i].setText("");
                    } else {
                        pas2Coef[i] = new JTextField(null, 20);
                        pas2Coef[i].setText(" ");
                        pas2Coef[i].setBounds(y, 190, 15, 15);
                        y = y + 40;
                        contentPanel.add(pas2Coef[i]);
                        pas2Coef[i].setText("");
                    }
                }
            }

            if(src.equals(enterPas11))
            {
                int nrCoefPol1 = Integer.parseInt(pas1.getText());
                Polinom[] poli = new Polinom[nrCoefPol1];
                for(int i = 0; i< nrCoefPol1*2; i=i+2)
                {
                    int xx = Integer.parseInt(pas1Coef[i].getText());
                    int xxx = Integer.parseInt(pas1Coef[i+1].getText());
                    poli[i/2] = new Polinom(xx,xxx);
                    p1 = p1.adunare(poli[i/2]);
                }
                System.out.println(p1);
            }

            if(src.equals(enterPas22))
            {
                int nrCoefPol2 = Integer.parseInt(pas2.getText());
                Polinom[] poli = new Polinom[nrCoefPol2];
                for(int i = 0; i<nrCoefPol2*2; i=i+2)
                {
                    int xx =Integer.parseInt(pas2Coef[i].getText());
                    int xxx = Integer.parseInt(pas2Coef[i+1].getText());
                    poli[i/2] = new Polinom(xx,xxx);
                    p2 = p2.adunare(poli[i/2]);
                }
                System.out.println(p2);
            }

            if(src.equals(add))
            {
                answer.setText("");
                Polinom p3 = p1.adunare(p2);
                String p4 = "" + p3;
                answer.setText(p4);
            }

            if(src.equals(sub))
            {
                answer.setText("");
                Polinom p3 = p1.scadere(p2);
                String p4 = "" + p3;
                answer.setText(p4);
            }

            if(src.equals(multiply))
            {
                answer.setText("");
                Polinom p3 = p1.inmultit(p2);
                String p4 = "" + p3;
                answer.setText(p4);
            }

            if(src.equals(div))
            {
                answer.setText("");
                Polinom p3 = p1.impartit(p2);
                String p4 = "" + p3;
                answer.setText(p4);
            }

            if(src.equals(derivata1))
            {
                
                Polinom p3 = p1.derivare();
                Polinom p4 = p2.derivare();
                String p5 = "" + p3;
                String p6 = "" + p4;
                answer1.setText(p5);
                answer2.setText(p6);
            }
            
            if(src.equals(derivata2))
            {
                
                Polinom p3 = p1.derivare().derivare();
                Polinom p4 = p2.derivare().derivare();
                String p5 = "" + p3;
                String p6 = "" + p4;
                answer1.setText(p5);
                answer2.setText(p6);
            }
            
            if(src.equals(integrate))
            {	
            	Polinom p3 = p1.integrare();
            	Polinom p4 = p2.integrare();
            	String p5 = "" + p3;
            	String p6 = "" + p4;
            	answer3.setText(p5);
            	answer4.setText(p6);
            }
        }
    }
}
