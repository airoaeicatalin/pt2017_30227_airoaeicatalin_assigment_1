import javax.swing.*;

public class Main {

    
    public static void main(String[] args) {
      
        Gui gui = new Gui();
        gui.setLayout(null); 
        gui.setSize(600, 700);
        gui.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        gui.setVisible(true);
        gui.setResizable(false);
    }
}
