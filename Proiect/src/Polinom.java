public class Polinom {
    private int[] nrCoef;
    private int gradMax;

    // a * x^b
    public Polinom(int a, int b) {
        nrCoef = new int[b + 1];
        nrCoef[b] = a;
        gradMax = grad();
    }

    public Polinom(int[] nrCoef){
        this.nrCoef = nrCoef;
    }
    
    public Polinom(int gradMax){
        this.gradMax = gradMax;
    }
    
    public int getGradMax(){
        return  this.gradMax;
    }
    
    public void setnrCoef(int[] nrCoef) {
    	this.nrCoef = nrCoef;	
    	}
    
    public void setGradMax(int gradMax){
       this.gradMax = gradMax;
    }

    public int grad() {
        int d = 0;
        for (int i = 0; i < nrCoef.length; i++)
            if (nrCoef[i] != 0) d = i;
        return d;
    }

    //a + b
    public Polinom adunare(Polinom b) {
        Polinom a = this;
        Polinom c = new Polinom(0, Math.max(a.gradMax, b.gradMax));
        for (int i = 0; i <= a.gradMax; i++)
        {c.nrCoef[i] += a.nrCoef[i];}
        for (int i = 0; i <= b.gradMax; i++)
        {c.nrCoef[i] += b.nrCoef[i];}
        c.gradMax = c.grad();
        return c;
    }

    // (a - b)
    public Polinom scadere(Polinom b) {
        Polinom a = this;
        Polinom c = new Polinom(0, Math.max(a.gradMax, b.gradMax));
        for (int i = 0; i <= a.gradMax; i++)
        {c.nrCoef[i] += a.nrCoef[i];}
        for (int i = 0; i <= b.gradMax; i++)
        {c.nrCoef[i] -= b.nrCoef[i];}
        c.gradMax = c.grad();
        return c;
    }

    //(a * b)
    public Polinom inmultit(Polinom b) {
        Polinom a = this;
        Polinom c = new Polinom(0, a.gradMax + b.gradMax);
        for (int i = 0; i <= a.gradMax; i++)
            for (int j = 0; j <= b.gradMax; j++)
                c.nrCoef[i + j] += (a.nrCoef[i] * b.nrCoef[j]);
        c.gradMax = c.grad();
        return c;
    }

    public Polinom impartit(Polinom b) {
        Polinom a = this;
        if (a.gradMax < b.gradMax)
            return new Polinom(0,0);
        int coeficient = a.nrCoef[a.gradMax]/(b.nrCoef[b.gradMax]);
        int exponent = a.gradMax - b.gradMax;
        Polinom c = new Polinom(coeficient, exponent);
        return c.adunare((a.scadere(b.inmultit(c)).impartit(b)));
    }

    
    public Polinom derivare() {
        if (gradMax == 0) return new Polinom(0, 0);
        Polinom deriv = new Polinom(0, gradMax - 1);
        deriv.gradMax = gradMax - 1;
        for (int i = 0; i < gradMax; i++)
            deriv.nrCoef[i] = (i + 1) * nrCoef[i + 1];
        return deriv;
    }
    
    public Polinom integrare(){
    	Polinom integral = new Polinom(0, gradMax + 1);
    	integral.gradMax = gradMax + 1;
        for (int i = 1; i <= gradMax+1; i++)
        {
                integral.nrCoef[i] = (nrCoef[i - 1] / i);
        }
        return integral;
    }


    public String toString() {
        if (gradMax == 0) return "" + nrCoef[0];
        if (gradMax == 1) return nrCoef[1] + "x + " + nrCoef[0];
        String s = nrCoef[gradMax] + "x^" + gradMax;
        for (int i = gradMax - 1; i >= 0; i--) {
            if (nrCoef[i] == 0) continue;
            else if (nrCoef[i] > 0) s = s + " + " + (nrCoef[i]);
            else if (nrCoef[i] < 0) s = s + " - " + (-nrCoef[i]);
            if (i == 1) s = s + "x";
            else if (i > 1) s = s + "x^" + i;
        }
        return s;
    }
}